package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/docker/go-connections/nat"
	"github.com/tkanos/gonfig"
	"golang.org/x/net/context"
)

//getAPIVersion : returns the API version
func getAPIVersion() string {

	//Read the configuration file
	configuration := Configuration{}
	err := gonfig.GetConf(configFile, &configuration)
	check(err)
	APIVersion := configuration.APIVersion
	return APIVersion
}

func addSecureHeaders(w http.ResponseWriter) http.ResponseWriter {
	w.Header().Set("content-type", "application/json")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-DNS-Prefetch-Control", "off")
	w.Header().Set("X-Frame-Options", "DENY")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	return w
}

//func checkHTTPMethod should be more compliant with http methods...
func checkHTTPMethod(r string, method string, w http.ResponseWriter) {
	if r != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		//w = addSecureHeaders(w)
		_, err := w.Write([]byte(`{"Method": "forbidden"}`))
		check(err)
		log.Println("Error in http method : " + r + " instead of " + method)
		panic("Error in http method !!")
	}
}

func validateContainerID(id string) error {
	var idRegex = regexp.MustCompile(`^[\w+-\.]{1,256}$`)
	if !idRegex.MatchString(id) {
		err := errors.New("ID: Invalid container ID")
		return err
	}
	return nil
}

func validateImageID(id string) error {
	var idRegex = regexp.MustCompile(`^sha256:[\w+-\.]{1,256}$`)
	if !idRegex.MatchString(id) {
		err := errors.New("ID: Invalid Image ID")
		return err
	}
	return nil
}

//func validateParams: validate Container parameters
func validateParams(c []Container, w http.ResponseWriter) error {
	var portRegex = regexp.MustCompile(`^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$`)
	var ipRegex = regexp.MustCompile(`^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$`)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	images, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		if err != nil {
			return errors.New("Error : Cannot write to w http.ResponseWriter")
		}
		return errors.New("Error : Cannot get image list")
	}

	for _, cont := range c {
		for _, image := range images {
			for _, tag := range image.RepoTags {
				if strings.Contains(tag, strings.TrimSpace(cont.ContainerName)) {
					if len(cont.ContainerName) == 0 || !portRegex.MatchString(cont.PortMapping.Guest) || !ipRegex.MatchString(cont.PortMapping.HostIP) {
						err := errors.New("Error : Empty parameter found")
						return err
					}
				} else if strings.TrimSpace(cont.ContainerName) == "" {
					w.WriteHeader(http.StatusForbidden)
					_, err = fmt.Fprintf(w, "Error : Empty image name")
					check(err)
				} else {
				}
			}
		}
	}
	return nil
}

//healthCheck func : a simple heartbeat
func healthCheck(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	//w.WriteHeader(http.StatusOK)
	w = addSecureHeaders(w)
	response := Response{"Alive : ok"}
	JSONResponse(response, w)

}

//Ping func : Check that Docker daemon is alive
func Ping(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	state, err := cli.Ping(context.Background())
	check(err)
	replyJSON, err := json.Marshal(state)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

//diskUsage func : get disk usage
func diskUsage(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	usage, err := cli.DiskUsage(context.Background())
	check(err)
	replyJSON, err := json.Marshal(usage)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}
}

//NewContainer constructor
func NewContainer(c *[]Container) *[]Container {
	//set default value for runtime to runc
	for _, cont := range *c {
		cont.Runtime = "runc"
	}
	return c
}

//startContainerWithPort func : start one or several containers and bind a port on host
//One port per container. Should be improve to support several port per container
func startContainerWithPort(c Container) {

	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	imageName := strings.TrimSpace(c.ContainerName)

	hostConfig := &container.HostConfig{
		PortBindings: nat.PortMap{
			nat.Port(strings.TrimSpace(c.PortMapping.Guest) + "/" + strings.TrimSpace(c.PortMapping.Proto)): []nat.PortBinding{
				{
					HostIP:   strings.TrimSpace(c.PortMapping.HostIP),
					HostPort: strings.TrimSpace(c.PortMapping.Host),
				},
			},
		},
		Runtime: c.Runtime,
	}

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image: imageName,
		ExposedPorts: nat.PortSet{
			nat.Port(c.PortMapping.Guest + "/" + c.PortMapping.Proto): struct{}{},
		},

		//Cmd : []string {"sh -c '/usr/sbin/sshd'"},
	}, hostConfig, nil, "")
	if err != nil {
		panic(err)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}
	log.Printf("Started container %v, ID : %v", c.ContainerName, resp.ID)
}

//ListContainers func : list all runnning containers
func ListContainers(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{})
	check(err)
	//var m []Container
	replyJSON, err := json.Marshal(containers)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}
}

//ListImages func : list available images
func ListImages(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	images, err := cli.ImageList(context.Background(), types.ImageListOptions{})
	check(err)
	replyJSON, err := json.Marshal(images)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

//stop func : stop a running container by ID
func stop(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var stopme ContainerID

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	err = json.NewDecoder(r.Body).Decode(&stopme)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}
	err = validateContainerID(stopme.ID)
	check(err)

	for _, container := range containers {
		if container.ID == stopme.ID {
			log.Print("Stopping container ", container.ID[:10], "... ")
			if err := cli.ContainerStop(ctx, container.ID, nil); err != nil {
				panic(err)
			}
			log.Println("...Success")
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Container": "stopped"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [STOP] container not found: " + stopme.ID)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container": "not found"}`))
	check(err)
}

//stopAll func : stop all running containers
func stopAll(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)

	for _, container := range containers {
		log.Print("Stopping container ", container.ID[:10], "... ")
		if err := cli.ContainerStop(ctx, container.ID, nil); err != nil {
			panic(err)
		}
		log.Println("...Success")
	}
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Containers": "stopped"}`))
	check(err)
}

func launch(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	log.Println(string(body))
	var t []Container
	tdef := NewContainer(&t)
	err = json.Unmarshal(body, &tdef)
	if err != nil {
		panic(err)
	}
	fmt.Println(tdef)
	log.Printf("%+v", t)
	err = validateParams(t, w)
	check(err)

	for _, Container := range t {
		//fmt.Println(Container)
		startContainerWithPort(Container)
	}
	//w.WriteHeader(http.StatusOK)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container": "started"}`))
	check(err)
}

//pullImage func : pull an image from docker hub
func pullImage(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	var img Image
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	err = json.NewDecoder(r.Body).Decode(&img)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}

	out, err := cli.ImagePull(ctx, img.ImageName, types.ImagePullOptions{})
	if err != nil {
		log.Println("Error : [STOP] Image not found: " + img.ImageName)
		w = addSecureHeaders(w)
		_, err = w.Write([]byte(`{"Image": "not found"}`))
		check(err)
		return
	}

	defer out.Close()

	_, err = io.Copy(os.Stdout, out)
	check(err)
	log.Println("Pulling an image : " + img.ImageName)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Image":"pulling"}`))
	check(err)
}

//prune func : delete all containers
func prune(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	noFilter := struct {
		filters             filters.Args
		expectedQueryParams map[string]string
	}{

		filters: filters.Args{},
		expectedQueryParams: map[string]string{
			"until":   "",
			"filter":  "",
			"filters": "",
		},
	}

	_, err = cli.ContainersPrune(ctx, noFilter.filters)
	if err != nil {
		panic(err)
	}
	log.Println("Pruning all containers...")
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Containers":"pruned"}`))
	check(err)
}

//kill func : Kill a container
func kill(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var killme ContainerID

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	err = json.NewDecoder(r.Body).Decode(&killme)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}
	err = validateContainerID(killme.ID)
	check(err)

	for _, container := range containers {
		if container.ID == killme.ID {

			err := cli.ContainerKill(ctx, killme.ID, "SIGKILL")
			if err != nil {
				panic(err)
			}
			log.Println("Killing container : " + killme.ID)
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Container":"killed"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [Killing] container not found: " + killme.ID)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container":"not found"}`))
	check(err)
}

//pause func : Pause a container
func pause(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var pauseme ContainerID

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	err = json.NewDecoder(r.Body).Decode(&pauseme)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}
	err = validateContainerID(pauseme.ID)
	check(err)
	for _, container := range containers {
		if container.ID == pauseme.ID {

			err := cli.ContainerPause(ctx, pauseme.ID)
			if err != nil {
				panic(err)
			}

			log.Println("Container paused: " + pauseme.ID)
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Container":"paused"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [PAUSE] Container no found : " + pauseme.ID)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container":"no found"}`))
	check(err)
}

//unpause func : Resume a container
func unpause(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var unpauseme ContainerID

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	err = json.NewDecoder(r.Body).Decode(&unpauseme)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}
	err = validateContainerID(unpauseme.ID)
	check(err)

	for _, container := range containers {
		if container.ID == unpauseme.ID {

			err := cli.ContainerUnpause(ctx, unpauseme.ID)
			if err != nil {
				panic(err)
			}

			log.Println("Container unpaused: " + unpauseme.ID)
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Container":"unpaused"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [Unpause] Container not found: " + unpauseme.ID)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container":"not found"}`))
	check(err)
}

//imageRemove func : Remove an image
func imageRemove(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodDelete, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	id, ok := r.URL.Query()["id"]

	if !ok || len(id[0]) < 1 {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		log.Println("Url Param id is missing for DELETE image")
		return
	}
	err = validateImageID(id[0])
	check(err)

	images, err := cli.ImageList(ctx, types.ImageListOptions{})
	//check(err)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}

	options := types.ImageRemoveOptions{
		Force:         true,
		PruneChildren: true,
	}

	for _, image := range images {
		if image.ID == id[0] {
			dels, err := cli.ImageRemove(ctx, id[0], options)
			if err != nil {
				panic(err)
			} else {
				for _, del := range dels {
					if del.Deleted != "" {
						log.Println("Deleted: " + del.Deleted)
					} else {
						//log.Println("Untagged: %s\n", del.Untagged)
						log.Println("Untagged: " + del.Untagged)

					}
				}
			}
			//log.Println("Removed image : " + dels)
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Image":"removed"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [Remove] Image not found: " + id[0])
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Image":"not found"}`))
	check(err)
}

//restart func : Restart a container
func restart(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)

	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var restartme ContainerID

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	err = json.NewDecoder(r.Body).Decode(&restartme)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}
	err = validateContainerID(restartme.ID)
	check(err)

	for _, container := range containers {
		if container.ID == restartme.ID {
			err = cli.ContainerRestart(ctx, restartme.ID, nil)
			if err != nil {
				panic(err)
			}

			log.Println("Restarting container : " + restartme.ID)
			w = addSecureHeaders(w)
			_, err := w.Write([]byte(`{"Container":"restarting"}`))
			check(err)
			return
		}
	}
	log.Println("Container restarted: " + restartme.ID)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container":"not found"}`))
	check(err)
}

//func info: info on the docker server
func info(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	infos, err := cli.Info(context.Background())
	check(err)
	//var m []Container
	replyJSON, err := json.Marshal(infos)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

//pause func : Pause a container
func containerInspect(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	id, ok := r.URL.Query()["id"]

	if !ok || len(id[0]) < 1 {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		log.Println("Url Param id is missing for INSPECT container")
		return
	}
	err = validateContainerID(id[0])
	check(err)
	inspectme := strings.TrimSpace(id[0])

	containers, err := cli.ContainerList(ctx, types.ContainerListOptions{})
	check(err)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}

	for _, container := range containers {
		if container.ID == inspectme {

			inspect, err := cli.ContainerInspect(ctx, inspectme)
			if err != nil {
				panic(err)
			}
			replyJSON, err := json.Marshal(inspect)
			check(err)
			log.Println("Container inspected: " + inspectme)
			w = addSecureHeaders(w)
			_, err = w.Write(replyJSON)
			check(err)
			return
		}
	}
	log.Println("Error : [INSPECT] Container no found : " + inspectme)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Container":"no found"}`))
	check(err)
}

//func netCreate: Creates a new network
//Should check if net exists before creating!!!
func netCreateBridge(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	/*type NetworkCreate struct {
		CheckDuplicate bool
		Driver         string
		Scope          string
		EnableIPv6     bool
		IPAM           *network.IPAM
		Internal       bool
		Attachable     bool
		Ingress        bool
		ConfigOnly     bool
		ConfigFrom     *network.ConfigReference
		Options        map[string]string
		Labels         map[string]string
	}*/
	var newnet Network
	err = json.NewDecoder(r.Body).Decode(&newnet)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Cannot parse your json")
		check(err)
		return
	}
	opt := types.NetworkCreate{
		CheckDuplicate: true,
		Driver:         "bridge",
	}
	_, err = cli.NetworkCreate(context.Background(), newnet.NetworkName, opt)
	//fmt.Println(response)
	//fmt.Println(err)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := fmt.Fprintf(w, "Error while creating network")
		check(err)
	} else {

		log.Println("Network created : " + newnet.NetworkName)
		w = addSecureHeaders(w)
		_, err := w.Write([]byte(`{"Network":"created"}`))
		check(err)
	}
}

//func netCreate: Creates a new network
//Should check if net exists before creating!!!
func netCreateSwarm(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	/*type NetworkCreate struct {
		CheckDuplicate bool
		Driver         string
		Scope          string
		EnableIPv6     bool
		IPAM           *network.IPAM
		Internal       bool
		Attachable     bool
		Ingress        bool
		ConfigOnly     bool
		ConfigFrom     *network.ConfigReference
		Options        map[string]string
		Labels         map[string]string
	}*/
	var newnet Network
	err = json.NewDecoder(r.Body).Decode(&newnet)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Cannot parse your json")
		check(err)
		return
	}
	opt := types.NetworkCreate{
		CheckDuplicate: true,
		Driver:         "overlay",
		//Ingress:        true,
		//Scope:          "swarm",
		Internal:   false,
		Attachable: true,
	}
	_, err = cli.NetworkCreate(context.Background(), newnet.NetworkName, opt)
	//fmt.Println(response)
	fmt.Println(err)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := fmt.Fprintf(w, "Error while creating network")
		check(err)
	} else {

		log.Println("Network created : " + newnet.NetworkName)
		w = addSecureHeaders(w)
		_, err := w.Write([]byte(`{"Network":"created"}`))
		check(err)
	}
}

//func netList: list all networks
func networkList(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	var opt types.NetworkListOptions
	infos, err := cli.NetworkList(context.Background(), opt)
	check(err)
	//var m []Container
	replyJSON, err := json.Marshal(infos)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

//imageRemove func : Remove an image
func networkRemove(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodDelete, w)
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	id, ok := r.URL.Query()["id"]
	idClean := strings.TrimSpace(id[0])
	if !ok || len(id[0]) < 1 {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		log.Println("Url Param id is missing for DELETE network")
		return
	}
	err = validateContainerID(idClean)
	check(err)
	opt := types.NetworkListOptions{}
	networks, err := cli.NetworkList(ctx, opt)
	//check(err)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}

	for _, network := range networks {
		if network.ID == id[0] {
			err := cli.NetworkRemove(ctx, idClean)
			if err != nil {
				panic(err)
			}
			log.Println("Removed network : " + idClean)
			w = addSecureHeaders(w)
			_, err = w.Write([]byte(`{"Network":"removed"}`))
			check(err)
			return
		}
	}
	log.Println("Error : [Remove] Network not found: " + idClean)
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Network":"not found"}`))
	check(err)
}

//networkConnect func : Add a network on a container
func networkConnect(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)

	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var conn NetConnect
	err = json.NewDecoder(r.Body).Decode(&conn)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Cannot parse your json")
		check(err)
		return
	}
	err = validateContainerID(conn.ContainerID)
	check(err)
	err = validateContainerID(conn.NetworkID)
	check(err)
	var conf *network.EndpointSettings
	err = cli.NetworkConnect(ctx, strings.TrimSpace(conn.NetworkID), strings.TrimSpace(conn.ContainerID), conf)
	fmt.Println(err)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err = fmt.Fprintf(w, "Internal error while connecting to network")
		check(err)
		return
	}
	log.Println("Attaching container " + strings.TrimSpace(conn.ContainerID) + " to network: " + strings.TrimSpace(conn.NetworkID))
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Network":"connected"}`))
	check(err)
}

//networkDiconnect func : Add a network on a container
func networkDisconnect(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)

	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var conn NetConnect
	err = json.NewDecoder(r.Body).Decode(&conn)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Cannot parse your json")
		check(err)
		return
	}
	err = validateContainerID(conn.ContainerID)
	check(err)
	err = validateContainerID(conn.NetworkID)
	check(err)
	err = cli.NetworkDisconnect(ctx, strings.TrimSpace(conn.NetworkID), strings.TrimSpace(conn.ContainerID), true)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err = fmt.Fprintf(w, "Internal error while diconnecting from network")
		check(err)
		return
	}
	log.Println("Disconnecting container " + strings.TrimSpace(conn.ContainerID) + " from network: " + strings.TrimSpace(conn.NetworkID))
	w = addSecureHeaders(w)
	_, err = w.Write([]byte(`{"Network":"disconnected"}`))
	check(err)
}

//func swarmInit: Init swarm manager node
func swarmInit(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	req := swarm.InitRequest{
		ListenAddr: "0.0.0.0",
	}
	swarmToken, err := cli.SwarmInit(context.Background(), req)
	check(err)
	mymap := map[string]string{"swarmtoken": swarmToken}
	replyJSON, err := json.Marshal(mymap)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

//func swarmInit: Init swarm manager node
func swarmLeave(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	err = cli.SwarmLeave(context.Background(), true)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write([]byte(`{"Swarm":"leaved"}`))
		check(err)
	}

}

//func swarmJoin: Join a swarm
func swarmJoin(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodPost, w)

	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)

	var joinReq swarm.JoinRequest
	err = json.NewDecoder(r.Body).Decode(&joinReq)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err := fmt.Fprintf(w, "Cannot parse your json")
		check(err)
		return
	}

	/*type JoinRequest struct {
		ListenAddr    string
		AdvertiseAddr string
		DataPathAddr  string
		RemoteAddrs   []string
		JoinToken     string // accept by secret
		Availability  NodeAvailability
	}*/
	err = cli.SwarmJoin(ctx, joinReq)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write([]byte(`{"Swarm":"Joined"}`))
		check(err)
	}

}

//func swarmInfo: Get info about the swarm
func swarmInfo(w http.ResponseWriter, r *http.Request) {
	checkHTTPMethod(r.Method, http.MethodGet, w)
	APIVersion := getAPIVersion()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	check(err)
	reply, err := cli.SwarmInspect(context.Background())
	check(err)
	log.Println("SWARM : REQUESTING INFO")
	replyJSON, err := json.Marshal(reply)
	if err != nil {
		log.Println("error:", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		//return
	} else {
		w = addSecureHeaders(w)
		_, err = w.Write(replyJSON)
		check(err)
	}

}

/*
func run(w http.ResponseWriter, r *http.Request) {
	APIVersion := getAPIVersion()
	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.WithVersion(APIVersion))
	if err != nil {
		panic(err)
	}

	//reader, err := cli.ImagePull(ctx, "kali-forensic", types.ImagePullOptions{})
	//if err != nil {
	//	panic(err)
	//}
	//io.Copy(os.Stdout, reader)

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image: "vulnerables/web-dvwa",
		//Cmd:   []string{"/bin/bash"},
		Tty: true,
	}, nil, nil, "")
	check(err)

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			panic(err)
		}
	case <-statusCh:
	}

	out, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true})
	check(err)

	io.Copy(os.Stdout, out)
	w.Header().Set("content-type", "application/json")
	w.Write([]byte(`{"Starting": "scenario1"}`))
}
*/

/*
//startContainer stupid func to print out things
func startContainer(c Container) bool {

	fmt.Println(c)
	//fmt.Println(reflect.TypeOf(c.ContainerName))
	//fmt.Println(c.PortMapping.Proto)
	//fmt.Println(reflect.TypeOf(c.PortMapping.Proto))
	//fmt.Println(reflect.TypeOf(c.PortMapping.Guest + "/" + c.PortMapping.Proto))
	fmt.Println(c.PortMapping.Guest + "/" + c.PortMapping.Proto)
	return true
}*/
