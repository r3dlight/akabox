package main

import (
	"encoding/json"
	"log"
	"net/http"
)

//JSONResponse func : Marshal an interface and write the json to the writer
func JSONResponse(response interface{}, w http.ResponseWriter) {

	json, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("content-type", "application/json")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-DNS-Prefetch-Control", "off")
	w.Header().Set("X-Frame-Options", "DENY")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Add("Strict-Transport-Security", "max-age=63072000; includeSubDomains")
	_, err = w.Write(json)
	check(err)
}

//Check func : generic error handling
func check(err error) {

	if err != nil {
		log.Println(err)
		panic(err)
	}
}
