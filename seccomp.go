package main

import (
	"fmt"
	"log"
	"syscall"

	libseccomp "github.com/seccomp/libseccomp-golang"
)

//whiteList func : init the syscall whitelisted
func whiteList(syscalls []string) {

	log.Println("Applying seccomp whitelist !")
	filter, err := libseccomp.NewFilter(libseccomp.ActErrno.SetReturnCode(int16(syscall.EPERM)))
	if err != nil {
		log.Printf("Error creating filter: %s\n", err)
		fmt.Printf("Error creating filter: %s\n", err)

	}
	for _, element := range syscalls {
		//log.Printf("[+] Whitelisting: %s\n", element)
		syscallID, err := libseccomp.GetSyscallFromName(element)
		if err != nil {
			panic(err)
		}
		err = filter.AddRule(syscallID, libseccomp.ActAllow)
		check(err)
	}
	err = filter.Load()
	check(err)
}
