package main

import (
	"net/http"

	"github.com/urfave/negroni"
)

func defineImagesEnpoints() {

	//GET : List all images
	http.Handle("/akabox/v0.1/images/listall", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(ListImages)),
	))
	//POST : Pull a container from docker hub
	http.Handle("/akabox/v0.1/image/pull", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(pullImage)),
	))
	//DELETE : Remove an image : /akabox/v0.1/image/remove?id=sha256:xxxxxx
	http.Handle("/akabox/v0.1/image/remove", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(imageRemove)),
	))
}
