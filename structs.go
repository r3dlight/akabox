package main

import jwt "github.com/dgrijalva/jwt-go"

//Configuration struct to read the config.json file
type Configuration struct {
	Port       string
	IP         string
	Logfile    string
	Login      string
	Password   string
	APIVersion string
	TLScrt     string
	TLSprivkey string
	JWTprivkey string
	JWTpubkey  string
}

//Token : a simple string represents the token
type Token struct {
	Token string `json:"token"`
}

//CustomClaims : TokenType + CustomerInfo struct
type CustomClaims struct {
	*jwt.StandardClaims
	TokenType string
	CustomerInfo
}

//CustomerInfo :  Define some custom types were going to use within our tokens
type CustomerInfo struct {
	Name string
}

//UserCredentials structure for JWT
type UserCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//Image type struct from docker hub
type Image struct {
	ImageName string `json:"imagename"`
}

//Response : a simple string
type Response struct {
	Data string `json:"data"`
}

//PortMapping struct
//TODO: The following struc should be able to handle multiple portmapping
type PortMapping struct {
	Guest  string `json:"guest"`
	Proto  string `json:"proto"`
	Host   string `json:"host"`
	HostIP string `json:"hostip"`
}

//Container struct
type Container struct {
	ContainerName string `json:"containername"`
	Runtime       string
	PortMapping   PortMapping
}

//ContainerID struct to receive an ID and do something with it
type ContainerID struct {
	ID string `json:"id"`
}

//Network struct
type Network struct {
	NetworkName string `json:"networkname"`
}

//NetworkID struct
type NetworkID struct {
	ID string `json:"id"`
}

//NetConnect struct
type NetConnect struct {
	ContainerID string `json:"containerid"`
	NetworkID   string `json:"networkid"`
}
