package main

import (
	"testing"
)

func TestCheckGoVersion(t *testing.T) {
	// Test success cases.
	testCases := []struct {
		version string
		success bool
	}{
		{minGoVersion, true},
		{"1.6.8", false},
		{"1.5", false},
		{"0.1", false},
		{".1", false},
		{"junkfood", false},
	}

	for _, testCase := range testCases {
		err := checkGoVersion(testCase.version)
		if err != nil && testCase.success {
			t.Fatalf("Test %v, expected: success, got: %v", testCase, err)
		}
		if err == nil && !testCase.success {
			t.Fatalf("Test %v, expected: failure, got: success", testCase)
		}
	}
}
