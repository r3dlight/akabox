package main

import (
	"crypto/rsa"
	"io/ioutil"

	"github.com/dgrijalva/jwt-go"
	"github.com/tkanos/gonfig"
)

//initKeys func : init the rsa keypair for JWT
func initKeys() (*rsa.PrivateKey, *rsa.PublicKey) {

	var err error

	configuration := Configuration{}
	err = gonfig.GetConf(configFile, &configuration)
	check(err)
	privKeyPath := configuration.TLSprivkey
	pubKeyPath := configuration.TLScrt

	var (
		verifyKey *rsa.PublicKey
		signKey   *rsa.PrivateKey
	)

	signBytes, err := ioutil.ReadFile(privKeyPath)
	check(err)

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	check(err)
	err = signKey.Validate()
	check(err)

	verifyBytes, err := ioutil.ReadFile(pubKeyPath)
	check(err)

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	check(err)

	return signKey, verifyKey
}
