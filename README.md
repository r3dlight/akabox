[![Go Report Card](https://goreportcard.com/badge/gitlab.com/r3dlight/akabox)](https://goreportcard.com/report/gitlab.com/r3dlight/akabox)
[![Build Status](https://gitlab.com/r3dlight/akabox/badges/master/build.svg)](https://gitlab.com/r3dlight/akabox/commits/master)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://gitlab.com/r3dlight/akabox/commits/master)
[![GitHub license](https://img.shields.io/pypi/l/Django.svg)](https://gitlab.com/r3dlight/akabox/blob/master/LICENSE)
[![codebeat badge](https://codebeat.co/badges/e9b159b7-25ae-4d75-97c5-7359a4d84e4b)](https://codebeat.co/projects/gitlab-com-r3dlight-akabox-master)

<br/>

<div align="center">
<img src ="web/images/logov2.png" alt="Akabox" width=400px/>
</div>

</br></br>

<div align="center">
<h3><em>A fast, easy and reasonably secure way to deploy your containers.</em></h3>
</div>

<br>

<div align="center">
<img src ="screenshots/schemas.png" alt="Akabox schemas" width=700px/>
</div>

<br/>

### Frontend screenshot :
![alt text](screenshots/dashboard4.png "Akabox dashboard")


### Backend security features :

- Less external packages usage and more stdlib

- Do not directly expose your Docker daemon

- gVisor userland kernel support

- Secure Computing / Kernel sandbox :
  * Seccomp whitelist (Only 48 syscalls allowed on x86_64 & AARCH64)

- RS512 (RSASHA512) JSON Web Tokens authentication (JWT) 
  * Generate and use our own RSA keypair in the jwt folder

- TLS 1.2 with strong ciphersuites :
  * Excluded ciphers : CBC-SHA, RC4, 3DES, CBC-SHA256 and dangerous PKCS1-v1.5 RSA padding scheme.
  * Only using curves with constant-time implementation (X25519, P-256)
  * Generate and use your own crt/key configuration in the tls folder (see config.json)

- HTTPS Security headers
- Graceful server shutdowns on SIGTERM signals
- Tested against race conditions
- Tested with Clang-7 memory sanitizer
- Tested with gosec, errcheck and wfuzz

### TODOs :
- Add more tests
- Add some documentation
- Add bridge config for windows, storage and swarm support
- Add support for TLS 1.3 with tls-tris when stable


#### Note : 
*As Continuous integration is now working fine (see pipeline), compiled ELF binary is available under download section -> artefact*
