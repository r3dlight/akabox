package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/tkanos/gonfig"
)

//ProtectedHandler : test route to see of the token is working
func ProtectedHandler(w http.ResponseWriter, r *http.Request) {

	response := Response{"Gained access to protected resource"}
	JSONResponse(response, w)

}

//LoginHandler : Authenticate the user a create a jwt
func LoginHandler(w http.ResponseWriter, r *http.Request) {

	configuration := Configuration{}
	err := gonfig.GetConf(configFile, &configuration)
	check(err)

	var user UserCredentials
	signKey, _ := initKeys()

	//decode request into UserCredentials struct
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		_, err = fmt.Fprintf(w, "Error in request")
		check(err)
		return
	}

	//fmt.Println(user.Username, user.Password)
	//fmt.Println(configuration.Login, configuration.Password)

	//validate user credentials
	if (strings.ToLower(user.Username) != configuration.Login) || (user.Password != configuration.Password) {
		log.Println("Invalid credentials")
		w.WriteHeader(http.StatusForbidden)
		fmt.Println("Error logging in")
		_, err = fmt.Fprint(w, "Invalid credentials")
		check(err)
		//return
	} else {

		// create a signer for rsa-sha512
		t := jwt.New(jwt.GetSigningMethod("RS512"))

		// set our claims
		t.Claims = &CustomClaims{
			&jwt.StandardClaims{
				// set the expire time
				// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
				ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
			},
			"access",
			CustomerInfo{configuration.Login},
		}

		check(err)
		tokenString, err := t.SignedString(signKey)
		check(err)
		fmt.Println(tokenString)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, err = fmt.Fprintln(w, "Error while signing the token")
			check(err)
			log.Printf("Error signing token: %v\n", err)
		}

		//create a token instance using the token string
		response := Token{tokenString}
		JSONResponse(response, w)

	}
}

//checkOptionsMethod middleware for negroni to handle OPTIONS method
//Sets Access-Control-Allow-Origin" to "*"
func checkOptionsMethodMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	if r.Method == "OPTIONS" {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		_, err := fmt.Fprint(w, "Allow: POST, GET, OPTIONS")
		check(err)
	} else {
		next(w, r)
	}
}

//ValidateTokenMiddleware func : token validation
func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	//validate token
	token, err := request.ParseFromRequest(r, request.AuthorizationHeaderExtractor,
		func(token *jwt.Token) (interface{}, error) {
			_, verifyKey := initKeys()
			/*fmt.Println(token.Claims)
			fmt.Println(token.Header)
			fmt.Println(token.Signature)
			fmt.Println(token.Raw)
			fmt.Println(token.Method)
			fmt.Println(verifyKey)
			fmt.Println(token.Valid)
			//fmt.Println(err)*/
			//fmt.Println(r.Method)
			//fmt.Println(token.Method)
			return verifyKey, nil
		})

	if err == nil {

		if token.Valid {

			next(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			_, err = fmt.Fprint(w, "Token is not valid")
			check(err)
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Println(err)
		_, err = fmt.Fprint(w, "Unauthorised access to this resource")
		check(err)
	}

}
