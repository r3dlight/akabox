package main

import (
	"net/http"

	"github.com/urfave/negroni"
)

func defineNetEnpoints() {

	//POST : Creates a new "bridge" network
	http.Handle("/akabox/v0.1/net/createbridge", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(netCreateBridge)),
	))
	//POST : Creates a new "swarm" network
	http.Handle("/akabox/v0.1/net/swarm/create", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(netCreateSwarm)),
	))
	//GET : List all networks
	http.Handle("/akabox/v0.1/net/listall", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(networkList)),
	))
	//DELETE : Delete a network ?id=xxxx"
	http.Handle("/akabox/v0.1/net/remove", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(networkRemove)),
	))
	//POST : Connect a container to a new network (add a new eth card) {"containerid":"xxxx","networkid","yyyy"}
	http.Handle("/akabox/v0.1/net/connect", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(networkConnect)),
	))
	//POST : Disconnect a container from a network (remove the eth card) {"containerid":"xxxx","networkid","yyyy"}
	http.Handle("/akabox/v0.1/net/disconnect", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(networkDisconnect)),
	))
	//GET : Init a swarm manager node, others nodes must join the swarm
	http.Handle("/akabox/v0.1/net/swarm/init", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(swarmInit)),
	))
	//GET : Leave the swarm manager mode
	http.Handle("/akabox/v0.1/net/swarm/leave", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(swarmLeave)),
	))
	//POST : Join a swarm as a client node with {joinToken : "AzErt"}
	http.Handle("/akabox/v0.1/net/swarm/join", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(swarmJoin)),
	))
	//GET : Info about the swarm
	http.Handle("/akabox/v0.1/net/swarm/info", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(swarmInfo)),
	))
}
