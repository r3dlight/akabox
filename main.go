package main

import (
	"fmt"
	"log"
	"os"
	"runtime"

	version "github.com/hashicorp/go-version"
	ak "gitlab.com/r3dlight/akabox/server"

	"github.com/tkanos/gonfig"
)

const (
	configFile          = "config.json"
	minGoVersion        = "1.10.4"
	goVersionConstraint = ">= " + minGoVersion
)

//Allowed syscalls for kernel sandboxing
var syscalls = []string{
	"futex", "write", "openat", "read", "epoll_ctl", "rt_sigprocmask", "clone",
	"fstat", "mmap", "mprotect", "munmap", "brk", "rt_sigaction", "access", "close", "execve",
	"fcntl", "sigaltstack", "arch_prctl", "gettid", "sched_getaffinity", "set_tid_address", "readlinkat",
	"set_robust_list", "epoll_create1", "prlimit64", "rt_sigreturn", "pselect6", "epoll_wait", "epoll_pwait",
	"fork", "vfork", "unshare", "exit", "wait4", "waitid", "exit_group", "sched_yield", "setsockopt", "connect",
	"accept4", "socket", "access", "getsockname", "getrandom", "getpeername", "bind", "listen"}

// Check if this binary is compiled with at least minimum Go version.
func checkGoVersion(goVersionStr string) error {
	constraint, err := version.NewConstraint(goVersionConstraint)
	if err != nil {
		return fmt.Errorf("'%s': %s", goVersionConstraint, err)
	}

	goVersion, err := version.NewVersion(goVersionStr)
	if err != nil {
		return err
	}

	if !constraint.Check(goVersion) {
		return fmt.Errorf("Akabox is not compiled by go %s. Minimum required version is %s, go %s release is known to have security issues. Please recompile accordingly", goVersionConstraint, minGoVersion, runtime.Version()[2:])
	}

	return nil
}

//main : main go routine
func main() {
	if err := checkGoVersion(runtime.Version()[2:]); err != nil {
		fmt.Println(err)
	}
	whiteList(syscalls)

	//Read the configuration file
	configuration := Configuration{}
	err := gonfig.GetConf(configFile, &configuration)
	check(err)
	httpIP := configuration.IP
	httpPort := configuration.Port
	LogFile := configuration.Logfile
	TLScrt := configuration.TLScrt
	TLSprivkey := configuration.TLSprivkey
	privKeyPath := configuration.JWTprivkey
	pubKeyPath := configuration.JWTpubkey

	//Logger setup
	f, err := os.OpenFile(LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	log.SetOutput(f)

	fmt.Println("Akabox server is starting...")
	log.Println("Akabox server is starting...")
	fmt.Println(privKeyPath)
	fmt.Println(pubKeyPath)
	//MOC variable to add in _test.go file
	//myjson := `[{"containername":"kali-ssh","portmapping" : {"guest": "22","proto": "tcp", "host": "2222","hostip":"0.0.0.0"}},{"containername":"tuxotron/xvwa","portmapping" : {"guest": "80","proto": "tcp","host": "2223","hostip":"0.0.0.0"}}]`
	//containers := getScenario(myjson)
	//fmt.Printf("%+v", containers)
	Start()

	srv := ak.NewTLSServer(httpIP, httpPort)

	err = srv.StartTLS(TLScrt, TLSprivkey)
	check(err)
}
