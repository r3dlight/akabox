PROJECT_NAME := "akabox"
PKG := "$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test

all: build

##lint: ## Lint the files
  #@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
  go test -short ${PKG_LIST}

race: dep ## Run data race detector
  go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
  go test -msan -short ${PKG_LIST}

dep: ## Get the dependencies
  go get -u github.com/codegangsta/negroni
  go get -u github.com/dgrijalva/jwt-go
  go get -u github.com/dgrijalva/jwt-go/request
  go get -u github.com/docker/docker/api/types
  go get -u github.com/docker/docker/api/types/container
  go get -u github.com/docker/docker/client
  go get -u github.com/docker/go-connections/nat
  go get -u github.com/hashicorp/go-version
  go get -u github.com/seccomp/libseccomp-golang
  go get -u github.com/tkanos/gonfig
  go get -u golang.org/x/net/context
  #cd $GOPATH/src/gitlab.com/r3dlight/akabox
  #mv $GOTPATH/src/github.com/docker/docker/vendor/github.com/docker/go-connections/{nat,nat.old}

build: dep ## Build the binary file
  go build -o akabox -i -v *.go

clean: ## Remove previous build
  @rm -f $(PROJECT_NAME)

help: ## Display this help screen
  @grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

