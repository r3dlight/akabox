package main

import (
	"net/http"

	"github.com/urfave/negroni"
)

func defineContainersEnpoints() {

	//GET : Stop all containers
	http.Handle("/akabox/v0.1/containers/stopall", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(stopAll)),
	))

	//POST : Launch containers from a json, add "runtime":"runsc" in json to run userland kernel
	http.Handle("/akabox/v0.1/containers/launch", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(launch)),
	))
	//POST : Stop just one container
	http.Handle("/akabox/v0.1/container/stop", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(stop)),
	))
	//GET : List all containers
	http.Handle("/akabox/v0.1/containers/listall", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(ListContainers)),
	))
	//GET : Caution !! will erase all containers (not images)
	http.Handle("/akabox/v0.1/containers/prune", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(prune)),
	))
	//POST : Kill (SIGKILL) a container {"id":"xxxx"}
	http.Handle("/akabox/v0.1/container/kill", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(kill)),
	))
	//POST : Pause a container {"id":"xxxx"}
	http.Handle("/akabox/v0.1/container/pause", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(pause)),
	))
	//POST : Unpause a container {"id":"xxxx"}
	http.Handle("/akabox/v0.1/container/unpause", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(unpause)),
	))

	//POST : Restart a container {"id":"xxxx"}
	http.Handle("/akabox/v0.1/container/restart", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(restart)),
	))
	//GET : Inspect a container ?id=xxx
	http.Handle("/akabox/v0.1/container/inspect", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(containerInspect)),
	))
}
