package main

//Start func : get config and start endpoints
func Start() {
	defineHostEnpoints()
	defineNetEnpoints()
	defineImagesEnpoints()
	defineContainersEnpoints()
}
