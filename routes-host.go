package main

import (
	"net/http"

	"github.com/urfave/negroni"
)

func greet(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "/var/www/web/index.html")
}

func defineHostEnpoints() {

	http.HandleFunc("/", greet)

	//TODO :
	//I should use with() instead of repeating the same middleware !!!
	//POST : Login + Password to get a token back
	//http.HandleFunc("/akabox/v0.1/login", LoginHandler)

	http.Handle("/akabox/v0.1/login", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.Wrap(http.HandlerFunc(LoginHandler)),
	))
	/**********PROTECTED ENDPOINTS**********/

	//GET : check if token is actually working
	http.Handle("/akabox/v0.1/", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(ProtectedHandler)),
	))
	//GET : A simple heartbeat
	http.Handle("/akabox/v0.1/health", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(healthCheck)),
	))
	//GET : Get some host infos
	http.Handle("/akabox/v0.1/host/infos", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(info)),
	))
	//GET : Diskusage by the daemon
	http.Handle("/akabox/v0.1/host/diskusage", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(diskUsage)),
	))
	//GET :  State of the docker daemon
	http.Handle("/akabox/v0.1/host/ping", negroni.New(
		negroni.HandlerFunc(checkOptionsMethodMiddleware),
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(Ping)),
	))
}
