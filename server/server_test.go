package server

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"testing"
	"time"
)

// Test isHTTPMethod()
func TestIsHTTPMethod(t *testing.T) {
	testCases := []struct {
		method         string
		expectedResult bool
	}{
		{"", false},
		{"get", false},
		{"put", false},
		{"UPLOAD", false},
		{"OPTIONS", true},
		{"GET", true},
		{"HEAD", true},
		{"POST", true},
		{"PUT", true},
		{"DELETE", true},
		{"TRACE", true},
		{"CONNECT", true},
		{"PRI", true},
	}

	for _, testCase := range testCases {
		result := isHTTPMethod(testCase.method)
		if result != testCase.expectedResult {
			t.Fatalf("expected: %v, got: %v", testCase.expectedResult, result)
		}
	}
}

//Must refactor my server : New, Start & Shutdown to stop it from the test file
func TestTLSServer(t *testing.T) {
	var unsupportedCipherSuites = []uint16{
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,    // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256, // No countermeasures against timing attacks
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA,    // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA,        // Broken cipher
		tls.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA,     // Sweet32
		tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,      // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,   // No countermeasures against timing attacks
		tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,      // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_ECDHE_RSA_WITH_RC4_128_SHA,          // Broken cipher

		// all RSA-PKCS1-v1.5 ciphers are disabled - danger of Bleichenbacher attack variants
		tls.TLS_RSA_WITH_3DES_EDE_CBC_SHA,   // Sweet32
		tls.TLS_RSA_WITH_AES_128_CBC_SHA,    // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_RSA_WITH_AES_128_CBC_SHA256, // No countermeasures against timing attacks
		tls.TLS_RSA_WITH_AES_256_CBC_SHA,    // Go stack contains (some) countermeasures against timing attacks (Lucky13)
		tls.TLS_RSA_WITH_RC4_128_SHA,        // Broken cipher

		tls.TLS_RSA_WITH_AES_128_GCM_SHA256, // Disabled because of RSA-PKCS1-v1.5 - AES-GCM is considered secure.
		tls.TLS_RSA_WITH_AES_256_GCM_SHA384, // Disabled because of RSA-PKCS1-v1.5 - AES-GCM is considered secure.
	}

	testCases := []struct {
		ciphers     []uint16
		expectedErr bool
	}{
		{defaultCipherSuites, false},
		{unsupportedCipherSuites, true},
	}

	for _, testCase := range testCases {
		func() {
			fmt.Println(testCase)
			srv := NewTLSServer("127.0.0.1", "8080")

			go func() {
				err := srv.StartTLS("../tls/server.crt", "../tls/server.key")
				fmt.Println(err)

			}()
			defer func() {
				err := srv.Stop()
				if err != nil {
					log.Println(err)
					panic(err)
				}
			}()
			client := http.Client{
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true,
						CipherSuites:       testCase.ciphers,
					},
				},
			}

			// There is no guaranteed way to know whether the HTTP server is started successfully.
			// The only option is to connect and check.  Hence below sleep is used as workaround.
			time.Sleep(2 * time.Second)

			_, err := client.Get("https://127.0.0.1:8080/akabox/v0.1/login")
			fmt.Println(err)

			expectErr := (err != nil)
			fmt.Println(err)

			if expectErr != testCase.expectedErr {
				t.Fatalf("test: error")
			}
		}()
	}

}
