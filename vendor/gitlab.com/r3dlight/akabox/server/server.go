package server

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"golang.org/x/net/context"
)

// HTTP methods.
var methods = []string{
	http.MethodGet,
	http.MethodHead,
	http.MethodPost,
	http.MethodPut,
	http.MethodPatch,
	http.MethodDelete,
	http.MethodConnect,
	http.MethodOptions,
	http.MethodTrace,
	"PRI", // HTTP 2 method
}

func isHTTPMethod(s string) bool {
	for _, method := range methods {
		if s == method {
			return true
		}
	}

	return false
}

//Server type enhanced http.Server type to add more fiel later
type Server struct {
	http.Server
	//srvrMutex *sync.Mutex
}

// Secure Go implementations of modern TLS ciphers
// The following ciphers are excluded because:
//  - RC4 ciphers:              RC4 is broken
//  - 3DES ciphers:             Because of the 64 bit blocksize of DES (Sweet32)
//  - CBC-SHA256 ciphers:       No countermeasures against Lucky13 timing attack
//  - CBC-SHA ciphers:          Legacy ciphers (SHA-1) and non-constant time
//                              implementation of CBC.
//  - RSA key exchange ciphers: Disabled because of dangerous PKCS1-v1.5 RSA
//                              padding scheme. See Bleichenbacher attacks.
var defaultCipherSuites = []uint16{
	tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
	tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
	tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
	tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
	tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
	tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
}

// Go only provides constant-time implementations of Curve25519 and NIST P-256 curve.
var secureCurves = []tls.CurveID{tls.X25519, tls.CurveP256}

//NewTLSServer func:
// -Defines public and protected endpoints using negroni
// -Defines the TLS configuration and ciphersuites
func NewTLSServer(httpIP string, httpPort string) *Server {

	tlscfg := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         secureCurves,
		PreferServerCipherSuites: true,
		CipherSuites:             defaultCipherSuites,
	}
	httpServer := &Server{}
	httpServer.Addr = httpIP + ":" + httpPort
	//Handler: http
	httpServer.TLSConfig = tlscfg
	httpServer.TLSNextProto = make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0)

	return httpServer
}

//StartTLS func : start in TLS mode and handle SIGTERM signal
func (s *Server) StartTLS(TLScrt string, TLSprivkey string) error {
	//handle server interrupts with channel and func literal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		log.Println("Gracefully stopping server...")
		//s.srvrMutex.Unlock()
		os.Exit(1)
	}()
	//s.srvrMutex.Lock()
	log.Println("Now listening...")
	//log.Fatal(s.ListenAndServeTLS(TLScrt, TLSprivkey))
	err := s.ListenAndServeTLS(TLScrt, TLSprivkey)
	return err
}

//Stop func : Stop the server gracefully
func (s *Server) Stop() error {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := s.Shutdown(ctx)
	return err
}
